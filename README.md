# cgpi

Códigos desenvolvidos para a disciplina de Computação Gráfica e Processamento de Imagem - CGPI.


## Infos
 * Made in OS : Windows 7
 * Tested in OS : Windows 7
 * Language Used : Java
 * Dependencies : NetBeans IDE
 * License : BSD 3-Clause License


## Contributors
 * Fernando A. Damião - [GitHub](https://github.com/fadamiao) - [Bitbucket](https://bitbucket.org/fadamiao)
 * André Boutros - [Bitbucket](https://bitbucket.org/aboutros)
 * Cristian Khalil - [Bitbucket](https://bitbucket.org/cristiankhalil)
 * Jonathan Luis Nogueira - [Bitbucket](https://bitbucket.org/jonathan_ti)
 * Ricardo Heil - [Bitbucket](https://bitbucket.org/Rickheil20)

/*Desenha 3 linhas de comprimentos diferentes: 100, 200 e 300 pixels,
 * separadas por uma distância de 20 pixels.
 * A primeira linha fica 20 pixels abaixo do limite superior da janela.
 * As linhas começam sempre na margem esquerda da tela (x=0).
 * Após isso a frase "Foram impressas 3 linhas de comprimentos 100, 200 e 300 pixels".
 * será escrita, 200 pixels abaixo,50 pixels à direita da margem.
 * A fonte das letras será Serif, Negrito, tamanho 14.
 * A janela será aberta com as dimensões largura = 500, altura = 400.
 * A origem da janela (canto superior esquerdo) está no canto superior esquerdo da tela.
 */

import java.awt.*; // para Graphics e Font
import javax.swing.*; // para JPanel e JFrame
class Painel3 extends JPanel
{
    public void paint(Graphics g)
    {
        int compr; // comprimento da linha
        int alt = 0; // altura da linha
        for (compr = 100; compr <= 300; compr += 100) {
            alt += 20;
            g.drawLine(0, alt, compr, alt);
        }
        Font font = new Font("Serif", Font.BOLD, 14); // cria a fonte para escrever a frase
        g.setFont(font); // estabelece a fonte que será usada a partir daqui.
        alt += 200; // define a altura 200 pixels abaixo da ultima linha desenhada.
        g.drawString("Foram impressas 3 linhas de comprimentos 100, 200 e 300 pixels", 50, alt);
    }
}

class Janela extends JFrame
{
    public Janela(String titulo)
    {
        super(titulo);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(new Painel3());
    }
}

public class DesenhaTresLinhas
{
    public static void main(String[] args)
    {
        Janela f = new Janela("Teste de Linhas");
        f.setSize(500, 400);
        f.setVisible(true);
    }
}

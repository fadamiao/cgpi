import java.awt.*;
import javax.swing.*;

class PainelT extends JPanel
{
    public void paint(Graphics g)
    {
        /*
         * p1, p1, p2, p2
         * p2, p2, p3, p3
         * p3, p3, p1, p1
         */
        g.drawLine(100, 100, 150, 100);
        g.drawLine(150, 100, 125, 50);
        g.drawLine(125, 50, 100, 100);
    }
}

class JanelaT extends JFrame
{
    public JanelaT(String titulo)
    {
        super(titulo);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(new PainelT());
    }
}

public class DesenhaTriangulo
{
    public static void main(String[] args)
    {
        JanelaT f = new JanelaT("Teste de Linhas");
        f.setSize(500, 400);
        f.setVisible(true);
    }
}

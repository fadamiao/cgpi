// Exemplo de uso da classe Graphics
// Adaptado do original de Frans Coenen
// Dept. of Comp. Sci., University of Liverpool
import java.awt.*; // Font, Color
import javax.swing.*;

class PainelC extends JPanel
{
    public void paint(Graphics g)
    {
        // Carroceria
        g.setColor(Color.blue);
        g.drawRect(50, 100, 180, 50);

        // Vidro
        g.setColor(Color.green);
        g.drawRect(90, 75, 100, 25);

        // Rodas
        g.setColor(Color.black);
        g.fillOval(60, 140, 30, 30);
        g.fillOval(185, 140, 30, 30);

        // Calotas
        int c1[] = {65, 155};
        int c2[] = {85, 155};
        int c3[] = {75, 145};
        g.setColor(Color.red);
        g.drawLine(c1[0], c1[1], c2[0], c2[1]);
        g.drawLine(c2[0], c2[1], c3[0], c3[1]);
        g.drawLine(c3[0], c3[1], c1[0], c1[1]);

        int c4[] = {190, 155};
        int c5[] = {210, 155};
        int c6[] = {200, 145};
        g.setColor(Color.red);
        g.drawLine(c4[0], c4[1], c5[0], c5[1]);
        g.drawLine(c5[0], c5[1], c6[0], c6[1]);
        g.drawLine(c6[0], c6[1], c4[0], c4[1]);

        Font serif = new Font("Serif", Font.BOLD, 18);
        g.setColor(Color.black);
        g.setFont(serif);
        g.drawString("Carro Java", 180, 50);
    }
}

class GuiC extends JFrame
{
    public GuiC(String text)
    {
        super(text);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(new PainelC());
    }
}

class DesenhaCarro
{
    public static void main(String[] args)
    {
        GuiC gui = new GuiC("Gato");
        gui.setSize(300, 300);
        gui.setVisible(true);
    }
}

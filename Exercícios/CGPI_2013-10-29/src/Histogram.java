/**
 *
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Cristian Khalil <cristiankhalil@gmail.com>
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @license BSD 3-Clause License
 * @see http://stackoverflow.com/a/2780607
 * 
 */

import java.awt.Color;
import java.awt.Graphics;
import java.util.Arrays;
import javax.swing.*;

class Panel extends JPanel
{
    int[][] m = new int[64][32];
    int[] h = new int[15];

    public Panel()
    {
        genHistogram();
        System.out.println("Mode: " + getMode());
        System.out.println("Median: " + getMedian());
        System.out.println("Average: " + getAverage());
    }

    @Override
    public void paint(Graphics g)
    {
        drawHistogram(g);
        drawImage(g, 40, 390);
    }

    private void genHistogram()
    {
        for (int i = 0; i < m.length; i++) 
        {
            for (int j = 0; j < m[0].length; j++) 
            {
                m[i][j] = (int) Math.round(Math.random() * 15);
            }
        }

        for (int k = 0; k < h.length; k++) 
        {
            for (int i = 0; i < m.length; i++) 
            {
                for (int j = 0; j < m[0].length; j++) 
                {
                    if(m[i][j] == k) 
                    {
                        h[k] += 1;
                    }
                }
            }
        }
    }

    private void drawHistogram(Graphics g)
    {
        int xPos = 40;
        for (int i = 0; i < h.length; i++) 
        {
            int yPos = 160 + (160 - h[i]);
            g.setColor(genGray(i));
            g.fillRect(xPos, yPos, 30, h[i]);
            xPos += 40;
        }
    }

    private void drawImage(Graphics g, int x, int y)
    {
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                g.setColor(genGray(m[i][j]));
                g.fillRect(x + i * 5, y + j * 5, 5, 5);
                g.setColor(Color.BLACK);
                g.drawRect(x + i * 5 - 1, y + j * 5 - 1, 5, 5);
            }
        }
    }

    private Color genGray(int num)
    {
        int numRGB = 255 - (int) ((num / 15.0) * 255.0);
        return new Color(numRGB, numRGB, numRGB);
    }
    
    private int getMode()
    {
        int maior = 0, mode = 0;
        for (int i = 0; i < h.length; i++)
        {
            if (h[i] > maior)
            {
                maior = h[i];
                mode = i;
            }
        }
        return mode;
    }
    
    private int getMedian()
    {
        int arr[] = new int[2048];
        int ind = 0;
        for (int i = 0; i < m.length; i++)
        {
            for (int j = 0; j< m[0].length; j++)
            {
                arr[ind] = m[i][j];
                ind++;
            }
        }
        Arrays.sort(arr);        
        return arr[(arr.length / 2) + 1];
    }
    
    private int getAverage()
    {
        int total = 0;
        for (int i = 0; i < m.length; i++)
        {
            for (int j = 0; j < m[0].length; j++)
            {
                total += m[i][j];
            }
        }
        return total / 2048;
    }
}

class Gui extends JFrame
{
    public Gui(String text)
    {
        super(text);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(new Panel());
    }
}

public class Histogram
{
    public static void main(String[] args)
    {
        Gui gui = new Gui("Histograma");
        gui.setSize(900, 600);
        gui.setVisible(true);
    }
}

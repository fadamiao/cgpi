import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.io.*;

class MoveTriang extends Component {

    int xm = 250, ym = 20;
    int lado; //   Triangulo_GUI myGUI = new Triangulo_GUI();
    MouseListener m;

    MoveTriang(int tamanho) {
        /****
         * O código abaixo usa uma classe local anônima para gerar o ouvinte de eventos de mouse.
         * O ouvinte será referenciado pela variável m.
         * Essa classe é sub-classe de MouseAdapter, e só redefine o método MouseReleased.
         * Os demais métodos da interface MouseListener continuarão vazios.
         ****/
        m = new MouseAdapter() {

            public void mouseReleased(MouseEvent e) {
                xm = e.getX();
                ym = e.getY();
                System.out.println("Coordenadas - x: " + xm + " - y: " + ym);
                repaint();
            }
        };
        addMouseListener(m);
    } // fim do construtor

    void desenhaTriang(Graphics g, int x, int y) {
        g.translate(x - 250, y - 20);
        g.drawLine(250, 20, 300, 40); //    inicio xm=250, ym=20;
        g.drawLine(250, 20, 140, 70);
        g.drawLine(140, 70, 300, 40);
    }

    public void paint(Graphics g) {
        desenhaTriang(g, xm, ym);
    }

    public static void main(String[] args) {

        JFrame f = new JFrame("Translada Triangs");
        f.setSize(400, 420);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(new MoveTriang(400));
        f.setVisible(true);
    }
}

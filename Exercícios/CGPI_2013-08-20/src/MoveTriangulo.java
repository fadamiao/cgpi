import java.awt.BorderLayout;  
import java.awt.Canvas;  
import java.awt.Color;  
import java.awt.Graphics;      
import java.awt.event.ActionEvent;  
import java.awt.event.ActionListener;    
  
import javax.swing.JButton;  
import javax.swing.JFrame;  
import javax.swing.JPanel;  
  
//@SuppressWarnings("serial")  
public class MoveTriangulo extends Canvas {  
  
   public static void main(String[] args) 
   {  
      new MoveTriangulo("mover triangulo");  
   }  
     
   private Color color = Color.BLUE;  
   //private Shape shape = null;  
   private int centerx = 250; // posicionamento de x da figura..  
   private int centery = 150; // posicionamento de y.. 
     
   public MoveTriangulo(String title) {  
     
      
      JButton btColor = new JButton("Cor");  
      btColor.addActionListener(new ActionListener() {  
         @Override  
         public void actionPerformed(ActionEvent e) {  
            if (color == Color.BLUE) {  
               color = Color.ORANGE;  
            } else if (color == Color.ORANGE) {  
               color = Color.RED;  
            } else {  
               color = Color.BLUE;  
            }  
            repaint();  
         }  
      });  
        
      JButton btLeft = new JButton("<<");  
      btLeft.addActionListener(new ActionListener() {  
         @Override  
         public void actionPerformed(ActionEvent e) {  
            //if (centerx > 70) {  
               centerx -= 20;  
               repaint();  
            //}  
         }  
      });  
        
      JButton btRight = new JButton(">>");  
      btRight.addActionListener(new ActionListener() {  
         @Override  
         public void actionPerformed(ActionEvent e) {  
            //if (centerx < getWidth()-70) {  
               centerx += 20;  
               repaint();  
            //}  
         }  
      });  
        
      JButton btdesce = new JButton("Desce");  
      btdesce.addActionListener(new ActionListener() {  
         @Override  
         public void actionPerformed(ActionEvent e) {  
            //if (centerx < getWidth()-70){  
               centery += 20;  
               repaint();  
            //}  
         }  
      });  
      
      JButton btsobe = new JButton("Sobe");  
      btsobe.addActionListener(new ActionListener() 
      {  
         @Override  
         public void actionPerformed(ActionEvent e) 
         {  
            //if (centerx < getWidth()-70){  
               centery -= 20;  
               repaint();  
            //}  
         }  
      });  
      
      JPanel panel = new JPanel();  
      //panel.add(btRect); 
      panel.add(btColor);  
      panel.add(btLeft);  
      panel.add(btRight);  
      panel.add(btdesce);  
      panel.add(btsobe); 
      
      JFrame frame = new JFrame(title);  
      frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);  
        
      frame.setLayout(new BorderLayout());// desenha a borda da figura  
      frame.add(this, BorderLayout.CENTER);  
      frame.add(panel, BorderLayout.SOUTH);  
      frame.setSize(800, 600);  
      frame.setLocationRelativeTo(null);  
      frame.setVisible(true);  
   }  
     
   @Override  
   public void paint(Graphics g) {  
       
         if (color != null) 
         {  
            g.setColor(color);  
         }  
         g.translate(centerx, centery); // move a figura ...   
         g.drawLine(50,200,250,200);
         g.drawLine(50,200,150,27);
         g.drawLine(250,200,150,27);  
   }  
}  
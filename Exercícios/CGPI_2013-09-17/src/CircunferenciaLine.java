/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @license BSD 3-Clause License
 * 
 */

import java.awt.*;
import javax.swing.*;

class PainelLine extends JPanel
{
    public void paint(Graphics g)
    {
        int i, xc, yc, x0, y0, R, np, x1, y1;
        double theta;

        i = 0;
        xc = 200;
        yc = 200;
        R = 100;
        x0 = xc - R;
        y0 = yc;

        np = 200;
        theta = Math.toRadians(360 / np);

        while (theta * i <= 2 * Math.PI) {
            x1 = xc - (int) (R * Math.cos(theta * i));
            y1 = yc + (int) (R * Math.sin(theta * i));
            g.drawLine(x0, y0, x1, y1);
            x0=x1; y0=y1;
            i++;
        }
    }
}

class GuiLine extends JFrame
{
    public GuiLine(String text)
    {
        super(text);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(new PainelLine());
    }
}

public class CircunferenciaLine
{
    public static void main(String[] args)
    {
        GuiLine gui = new GuiLine("Circunferência drawLine");
        gui.setSize(600, 600);
        gui.setVisible(true);
    }
}

/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @license BSD 3-Clause License
 * 
 */

public class Point2D
{
    float x, y;
    Point2D(double x, double y)
    {
        this.x = (float)x;
        this.y = (float)y;
    }
}

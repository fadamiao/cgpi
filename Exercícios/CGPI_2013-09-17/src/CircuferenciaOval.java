/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @license BSD 3-Clause License
 * 
 */

import java.awt.*;
import javax.swing.*;

class PainelOval extends JPanel
{
    public void paint(Graphics g)
    {
        int x0 = 200, y0 = 200, R = 100;
        g.drawOval(x0, y0, R * 2, R * 2);
    }
}

class GuiOval extends JFrame
{
    public GuiOval(String text)
    {
        super(text);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(new PainelOval());
    }
}

public class CircuferenciaOval
{
    public static void main(String[] args)
    {
        GuiOval gui = new GuiOval("Circunferência drawOval");
        gui.setSize(600, 600);
        gui.setVisible(true);
    }
}

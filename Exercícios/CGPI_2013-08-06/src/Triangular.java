import javax.swing.*;

public class Triangular
{
    private static float a, b, c;
    private static boolean eTriangulo = false;

    public static void main(String[] args)
    {
        LerDados();
        eTriangulo = AvaliarTriag (a,b,c);
        MostrarSaida(eTriangulo);
    }

    static void LerDados()
    {
        a = Float.parseFloat(JOptionPane.showInputDialog("digite lado A"));
        b = Float.parseFloat(JOptionPane.showInputDialog("digite lado B"));
        c = Float.parseFloat(JOptionPane.showInputDialog("digite lado C"));
    }

    static void MostrarSaida(Boolean etriangulo)
    {
        if (eTriangulo)
            System.out.println("Dados compoem um Triângulo!");
        else
            System.out.println("Dados NAO compoem um Triângulo!");
    }

    static boolean AvaliarTriag (float a, float b, float c)
    {
        if ((a+b) > c)
            return true;
        else
            return false;
    }
}
